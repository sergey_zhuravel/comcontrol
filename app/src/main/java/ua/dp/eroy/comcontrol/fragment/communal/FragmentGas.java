package ua.dp.eroy.comcontrol.fragment.communal;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Date;

import co.moonmonkeylabs.realmrecyclerview.RealmRecyclerView;
import io.realm.RealmResults;
import ua.dp.eroy.comcontrol.R;
import ua.dp.eroy.comcontrol.adapter.GasRVAdapter;
import ua.dp.eroy.comcontrol.model.Gas;
import ua.dp.eroy.comcontrol.utils.RealmController;


public class FragmentGas extends CommunalApp {

    private RealmResults<Gas> gasRealmResults;
    private RealmRecyclerView realmRecyclerView;

    private EditText etReadingsThisGas;
    private Button btnPriceAddColdWater;
    private TextView tvPriceGas;
    private GasRVAdapter gasRVAdapter;

    private TextView rate, readings;
    private ImageView iconCom;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_communal_selected, container, false);

        initView(view);

        btnPriceAddColdWater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etReadingsThisGas.getText().toString().equals("")) {
                    setSnackBar(view, R.string.field_empty);
                } else {
                    calcAndAddToDB();

                }
            }
        });



        return view;
    }

    private void initView(View view) {


        realmRecyclerView = (RealmRecyclerView) view.findViewById(R.id.realm_recycler_view);
        etReadingsThisGas = (EditText) view.findViewById(R.id.etReadingsThisHold);
        btnPriceAddColdWater = (Button) view.findViewById(R.id.btnPriceAddHoldWater);

        tvPriceGas = (TextView) view.findViewById(R.id.tvPriceHoldWater);
        rate = (TextView) view.findViewById(R.id.rate);
        readings = (TextView) view.findViewById(R.id.readings);
        iconCom = (ImageView) view.findViewById(R.id.tvIconCom);

//        icon gas
        iconCom.setImageResource(R.drawable.gas);

        gasRealmResults = realm.where(Gas.class).findAllSorted("id", false);
        gasRVAdapter = new GasRVAdapter(getActivity(), gasRealmResults, true, true);

        realmRecyclerView.setAdapter(gasRVAdapter);

//        set text title
        readings.setText(getResources().getText(R.string.readings_gas));
    }


    //    calculate gas
    private float getCalcGas(int previousValue, int thisValue) {
        float value = (thisValue - previousValue) * price.getGas();
        String text = "(" + thisValue + " - " + previousValue + ") " + " * " + price.getGas() + " = " + value;
        setSnackBar(view, text);
        tvPriceGas.setText(text);
        return value;
    }

    private void calcAndAddToDB() {
        int thisValue = Integer.parseInt(etReadingsThisGas.getText().toString());
        // начальное значение
        int previousValue = 0;
// find previous value in BD
        RealmResults<Gas> rr = realm.where(Gas.class).findAll();
        for (Gas gas : rr) {
            previousValue = gas.getValue();
        }
        float price = getCalcGas(previousValue, thisValue);
//                add to db
        insertToDBGas(thisValue, price);
//                reset editText
        etReadingsThisGas.setText("");
    }

    //    insert to db gas
    private void insertToDBGas(int value, float price) {
        Gas gas = new Gas();
        gas.setValue(value);
        gas.setPrice(price);
        gas.setMonth(String.valueOf(formatDate(new Date())));
        gas.setId(RealmController.getInstance().getColdWater().size() + System.currentTimeMillis());

        realm.beginTransaction();
        realm.copyToRealm(gas);
        realm.commitTransaction();
        gasRVAdapter.notifyDataSetChanged();

    }


    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String prices = prefs.getString("gas", "0.0");
        if (isNumeric(prices)) {
            float pr = Float.parseFloat(prices);
            price.setGas(pr);
        } else {
            setSnackBar(R.string.error_gas);
        }



        rate.setText(String.valueOf(price.getGas()));
    }

}
