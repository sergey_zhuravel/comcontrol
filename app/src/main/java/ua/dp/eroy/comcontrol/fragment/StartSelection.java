package ua.dp.eroy.comcontrol.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ua.dp.eroy.comcontrol.R;
import ua.dp.eroy.comcontrol.adapter.StartSelectionAdapter;
import ua.dp.eroy.comcontrol.model.Communal;


public class StartSelection extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;


    private List<Communal> communalList;
    private Button btnShow;


    @Override
    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.start_selection_communal, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        btnShow = (Button) view.findViewById(R.id.btnShow);

        communalList = new ArrayList<>();

        addCommunal();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        adapter = new StartSelectionAdapter(communalList);
        recyclerView.setAdapter(adapter);

        btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String data = "";

                List<Communal> stList = ((StartSelectionAdapter) adapter).getCommunalList();

                for (int i = 0; i < stList.size(); i++) {
                Communal singleCommunal = stList.get(i);
                    if (singleCommunal.isSelected()) {
                        data = data + "\n" + singleCommunal.getText().toString();
                    }
                }

                Toast.makeText(getContext(),"Selected Communal: \n" + data,Toast.LENGTH_LONG).show();

            }
        });


        return view;
    }

    private void addCommunal() {
        Communal communal = new Communal("ColdWater", R.drawable.com_cold_water, false);
        Communal communal1 = new Communal("HotWater", R.drawable.com_hot_water, false);
        Communal communal2 = new Communal("Gas", R.drawable.com_gas, false);
        Communal communal3 = new Communal("Energy", R.drawable.com_energy, false);
        Communal communal4 = new Communal("Lift", R.drawable.com_lift, false);
        Communal communal5 = new Communal("HeatEnergy", R.drawable.com_heat_energy, false);
        Communal communal6 = new Communal("Garbage", R.drawable.com_garbage, false);

        communalList.add(communal);
        communalList.add(communal1);
        communalList.add(communal2);
        communalList.add(communal3);
        communalList.add(communal4);
        communalList.add(communal5);
        communalList.add(communal6);
    }
}
