package ua.dp.eroy.comcontrol.utils;

/**
 * Created by serj on 26.07.2016.
 */
public class Const {
    public static final String COLD_WATER = "coldWater";
    public static final String HOT_WATER = "hotWater";
    public static final String ENERGY = "energy";
    public static final String GAS = "gas";
}
