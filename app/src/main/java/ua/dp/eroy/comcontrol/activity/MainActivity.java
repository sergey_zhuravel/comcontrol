package ua.dp.eroy.comcontrol.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import ua.dp.eroy.comcontrol.R;
import ua.dp.eroy.comcontrol.fragment.FragmentAbout;
import ua.dp.eroy.comcontrol.fragment.FragmentGraph;
import ua.dp.eroy.comcontrol.fragment.StartSelection;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



//        init toolbar
        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mActionBarToolbar);
        mActionBarToolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        mActionBarToolbar.setBackgroundColor(getResources().getColor(R.color.colorDark));
        mActionBarToolbar.setOverflowIcon(getResources().getDrawable(R.drawable.ic_more_vert_white_24dp));


//start screen
//        TODO: для тестов поменял с CommunalList на StartSelection

        MainActivity.this.getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new StartSelection()).commit();


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }


    // help options in menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_help:
                Intent intent = new Intent();
                intent.setClass(this, SetPreferenceActivity.class);
                startActivityForResult(intent, 0);
                break;

            case R.id.graph:
                MainActivity.this.getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new FragmentGraph()).addToBackStack(null).commit();
                break;

            case R.id.about:
                MainActivity.this.getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new FragmentAbout()).addToBackStack(null).commit();

                break;
        }
        return true;


    }

}
