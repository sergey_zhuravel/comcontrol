package ua.dp.eroy.comcontrol.fragment.communal;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.View;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.Realm;
import ua.dp.eroy.comcontrol.model.Price;
import ua.dp.eroy.comcontrol.utils.RealmController;


public class CommunalApp extends Fragment {
    public Price price;
    public Realm realm;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setRetainInstance(true);
        price = new Price();

        //get realm instance
        this.realm = RealmController.with(this).getRealm();
    }


    public static String formatDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM,yyyy");

        return dateFormat.format(date);
    }

    public void setSnackBar(String text) {
        Snackbar.make(getView(), text, Snackbar.LENGTH_LONG).show();
    }

    public void setSnackBar(int stringResource) {
        Snackbar.make(getView(), stringResource, Snackbar.LENGTH_LONG).show();
    }

    public void setSnackBar(View view, String text) {
        Snackbar.make(view, text, Snackbar.LENGTH_LONG).show();
    }

    public void setSnackBar(View view, int stringResource) {
        Snackbar.make(view, stringResource, Snackbar.LENGTH_LONG).show();
    }

    public static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }
}
