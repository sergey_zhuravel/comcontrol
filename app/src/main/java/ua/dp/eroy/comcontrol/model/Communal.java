package ua.dp.eroy.comcontrol.model;

import java.io.Serializable;

public class Communal implements Serializable {



    private String text;
    private int image;

    private boolean isSelected;

    public Communal() {
    }

    public Communal(String text, int image) {
        this.text = text;
        this.image = image;
    }

    public Communal(String text, int image, boolean isSelected) {
        this.text = text;
        this.image = image;
        this.isSelected = isSelected;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
