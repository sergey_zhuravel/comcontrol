package ua.dp.eroy.comcontrol.utils;

import android.app.Activity;
import android.app.Application;
import android.support.v4.app.Fragment;

import io.realm.Realm;
import io.realm.RealmResults;
import ua.dp.eroy.comcontrol.model.ColdWater;
import ua.dp.eroy.comcontrol.model.Energy;
import ua.dp.eroy.comcontrol.model.Gas;
import ua.dp.eroy.comcontrol.model.HotWater;


public class RealmController {
    private static RealmController instance;
    private final Realm realm;

    public RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController with(Fragment fragment) {

        if (instance == null) {
            instance = new RealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static RealmController with(Activity activity) {

        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }

    public static RealmController with(Application application) {

        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }

    public static RealmController getInstance() {

        return instance;
    }

    public Realm getRealm() {

        return realm;
    }


    //find all objects in the Communal
    public RealmResults<ColdWater> getColdWater() {

        return realm.where(ColdWater.class).findAll();
    }

    public RealmResults<HotWater> getHotWater() {

        return realm.where(HotWater.class).findAll();
    }

    public RealmResults<Energy> getEnergy() {

        return realm.where(Energy.class).findAll();
    }

    public RealmResults<Gas> getGas() {

        return realm.where(Gas.class).findAll();
    }





}
