package ua.dp.eroy.comcontrol.model;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ColdWater extends RealmObject {

    @PrimaryKey

    private long id;
    private int value;

    private String month;
    private float price;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
