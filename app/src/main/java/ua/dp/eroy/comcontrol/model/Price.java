package ua.dp.eroy.comcontrol.model;


public class Price {

    private float coldWater;
    private float hotWater;
    private float energyLess100;
    private float energyMore100;
    private float gas;

    public float getGas() {
        return gas;
    }

    public void setGas(float gas) {
        this.gas = gas;
    }


    public float getEnergyLess100() {
        return energyLess100;
    }

    public void setEnergyLess100(float energyLess100) {
        this.energyLess100 = energyLess100;
    }

    public float getEnergyMore100() {
        return energyMore100;
    }

    public void setEnergyMore100(float energyMore100) {
        this.energyMore100 = energyMore100;
    }


    public float getColdWater() {
        return coldWater;
    }

    public void setColdWater(float coldWater) {
        this.coldWater = coldWater;
    }

    public float getHotWater() {
        return hotWater;
    }

    public void setHotWater(float hotWater) {
        this.hotWater = hotWater;
    }
}
