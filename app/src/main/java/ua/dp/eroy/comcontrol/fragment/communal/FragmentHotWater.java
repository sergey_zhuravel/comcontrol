package ua.dp.eroy.comcontrol.fragment.communal;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Date;

import co.moonmonkeylabs.realmrecyclerview.RealmRecyclerView;
import io.realm.RealmResults;
import ua.dp.eroy.comcontrol.R;
import ua.dp.eroy.comcontrol.adapter.HotWaterRVAdapter;
import ua.dp.eroy.comcontrol.model.HotWater;
import ua.dp.eroy.comcontrol.utils.RealmController;


public class FragmentHotWater extends CommunalApp {


    private RealmResults<HotWater> hotWaterRealmResults;
    private RealmRecyclerView realmRecyclerView;

    private EditText etReadingsThisHot;
    private Button btnPriceAddHotWater;
    private TextView tvPriceHotWater;
    private HotWaterRVAdapter hotWaterRVAdapter;

    private TextView rate, readings;
    private ImageView iconCom;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_communal_selected, container, false);

        initView(view);

        btnPriceAddHotWater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etReadingsThisHot.getText().toString().equals("")) {
                    setSnackBar(view, R.string.field_empty);
                } else {
                    calcAndAddToDB();
                }
            }
        });


        return view;
    }

    private void initView(View view) {

        realmRecyclerView = (RealmRecyclerView) view.findViewById(R.id.realm_recycler_view);
        etReadingsThisHot = (EditText) view.findViewById(R.id.etReadingsThisHold);
        btnPriceAddHotWater = (Button) view.findViewById(R.id.btnPriceAddHoldWater);
        tvPriceHotWater = (TextView) view.findViewById(R.id.tvPriceHoldWater);
        rate = (TextView) view.findViewById(R.id.rate);
        readings = (TextView) view.findViewById(R.id.readings);
        iconCom = (ImageView) view.findViewById(R.id.tvIconCom);

        iconCom.setImageResource(R.drawable.hotwater);

        hotWaterRealmResults = realm.where(HotWater.class).findAllSorted("id", false);
        hotWaterRVAdapter = new HotWaterRVAdapter(getActivity(), hotWaterRealmResults, true, true);

        realmRecyclerView.setAdapter(hotWaterRVAdapter);

//        set text title
        readings.setText(getResources().getText(R.string.readings_hot_water));
    }


    private void calcAndAddToDB() {
        int thisValue = Integer.parseInt(etReadingsThisHot.getText().toString());
        //                    начальное значение
        int previousValue = 0;
// find previous value in BD
        RealmResults<HotWater> rr = realm.where(HotWater.class).findAll();
        for (HotWater hw : rr) {
            previousValue = hw.getValue();
        }
        float price = getCalcHotWater(previousValue, thisValue);

//                add to db
        insertToDBHotWater(thisValue, price);
//                reset editText
        etReadingsThisHot.setText("");
    }


    //    calculate cold water
    private float getCalcHotWater(int previousValue, int thisValue) {

        float value = (thisValue - previousValue) * price.getHotWater();
        String text = "(" + thisValue + " - " + previousValue + ") " + " * " + price.getHotWater() + " = " + value;
        setSnackBar(view, text);
        tvPriceHotWater.setText(text);

        return value;
    }

    //    insert to db HotWater
    private void insertToDBHotWater(int value, float price) {
        HotWater hotWater = new HotWater();
        hotWater.setValue(value);
        hotWater.setPrice(price);
        hotWater.setMonth(String.valueOf(formatDate(new Date())));
        hotWater.setId(RealmController.getInstance().getColdWater().size() + System.currentTimeMillis());

        realm.beginTransaction();
        realm.copyToRealm(hotWater);
        realm.commitTransaction();
        hotWaterRVAdapter.notifyDataSetChanged();
    }



    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        String prices = prefs.getString("hotwater", "0.0");
        if (isNumeric(prices)) {
            float pr = Float.parseFloat(prices);
            price.setHotWater(pr);
        } else {
            setSnackBar(R.string.error_hot_water);
        }


        //    set text rate cold water
        rate.setText(String.valueOf(price.getHotWater()));

    }
}
