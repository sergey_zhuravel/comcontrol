package ua.dp.eroy.comcontrol.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import io.realm.Realm;
import io.realm.RealmBasedRecyclerViewAdapter;
import io.realm.RealmResults;
import ua.dp.eroy.comcontrol.R;
import ua.dp.eroy.comcontrol.model.HotWater;
import ua.dp.eroy.comcontrol.utils.RealmController;

public class HotWaterRVAdapter extends RealmBasedRecyclerViewAdapter<HotWater, ViewHolderRecyclerView> {

    private Realm realm;
    private Context context;

    public HotWaterRVAdapter(Context context, RealmResults<HotWater> realmResults, boolean automaticUpdate, boolean animateResults) {
        super(context, realmResults, automaticUpdate, animateResults);

        this.context = context;
    }

    @Override
    public ViewHolderRecyclerView onCreateRealmViewHolder(ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.item_readings, viewGroup, false);

        return new ViewHolderRecyclerView((LinearLayout) view);
    }

    @Override
    public void onBindRealmViewHolder(ViewHolderRecyclerView viewHolderRecyclerView, int i) {
        realm = RealmController.getInstance().getRealm();

        final HotWater hotWater = realmResults.get(i);
        viewHolderRecyclerView.tvValue.setText(String.valueOf(hotWater.getValue()));
        viewHolderRecyclerView.tvPrice.setText(String.valueOf(hotWater.getPrice()));
        viewHolderRecyclerView.tvMonth.setText(String.valueOf(hotWater.getMonth()));


        viewHolderRecyclerView.ll.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                realm.beginTransaction();
                hotWater.removeFromRealm();
                realm.commitTransaction();

                notifyDataSetChanged();

                return false;
            }
        });

        viewHolderRecyclerView.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                View content = inflater.inflate(R.layout.edit_item_readings, null);
                final EditText editValue = (EditText) content.findViewById(R.id.etValue);
                final EditText editPrice = (EditText) content.findViewById(R.id.etPrice);

                editValue.setText(String.valueOf(hotWater.getValue()));
                editPrice.setText(String.valueOf(hotWater.getPrice()));

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setView(content)
                        .setTitle("Edit readings")
                        .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                realm.beginTransaction();
                                int value = Integer.parseInt(editValue.getText().toString());
                                float price = Float.parseFloat(editPrice.getText().toString());
                                hotWater.setValue(value);
                                hotWater.setPrice(price);
                                realm.commitTransaction();

                                notifyDataSetChanged();

                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return realmResults.size();
    }

}
