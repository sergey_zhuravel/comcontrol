package ua.dp.eroy.comcontrol.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class Energy extends RealmObject {
    @PrimaryKey

    private int value;

    private String month;
    private float price;
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }


    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
