package ua.dp.eroy.comcontrol.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import io.realm.Realm;
import io.realm.RealmBasedRecyclerViewAdapter;
import io.realm.RealmResults;
import io.realm.RealmViewHolder;
import ua.dp.eroy.comcontrol.R;
import ua.dp.eroy.comcontrol.model.ColdWater;
import ua.dp.eroy.comcontrol.utils.RealmController;


public class ColdWaterRVAdapter extends RealmBasedRecyclerViewAdapter<ColdWater, ViewHolderRecyclerView> {

    private Realm realm;
    private Context context;


    public ColdWaterRVAdapter(Context context, RealmResults<ColdWater> realmResults, boolean automaticUpdate, boolean animateResults) {

        super(context, realmResults, automaticUpdate, animateResults);
        this.context = context;
    }

    @Override
    public ViewHolderRecyclerView onCreateRealmViewHolder(ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.item_readings, viewGroup, false);

        return new ViewHolderRecyclerView((LinearLayout) view);
    }

    @Override
    public void onBindRealmViewHolder(ViewHolderRecyclerView viewHolderRecyclerView, final int i) {
        realm = RealmController.getInstance().getRealm();

        final ColdWater coldWater = realmResults.get(i);
        viewHolderRecyclerView.tvValue.setText(String.valueOf(coldWater.getValue()));
        viewHolderRecyclerView.tvPrice.setText(String.valueOf(coldWater.getPrice()));
        viewHolderRecyclerView.tvMonth.setText(String.valueOf(coldWater.getMonth()));


        viewHolderRecyclerView.ll.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                realm.beginTransaction();
                coldWater.removeFromRealm();
                realm.commitTransaction();

                notifyDataSetChanged();

                return false;
            }
        });

        viewHolderRecyclerView.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {


                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                View content = inflater.inflate(R.layout.edit_item_readings, null);
                final EditText editValue = (EditText) content.findViewById(R.id.etValue);
                final EditText editPrice = (EditText) content.findViewById(R.id.etPrice);

                editValue.setText(String.valueOf(coldWater.getValue()));
                editPrice.setText(String.valueOf(coldWater.getPrice()));

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setView(content)
                        .setTitle("Edit readings")
                        .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                realm.beginTransaction();
                                int value = Integer.parseInt(editValue.getText().toString());
                                float price = Float.parseFloat(editPrice.getText().toString());
                                coldWater.setValue(value);
                                coldWater.setPrice(price);
                                realm.commitTransaction();

                                notifyDataSetChanged();

                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });


                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });


    }


    @Override
    public int getItemCount() {
        return realmResults.size();
    }


}


class ViewHolderRecyclerView extends RealmViewHolder {
    public TextView tvValue;
    public TextView tvPrice;
    public TextView tvMonth;
    public LinearLayout ll;

    public ViewHolderRecyclerView(View itemView) {
        super(itemView);

        tvValue = (TextView) itemView.findViewById(R.id.value);
        tvPrice = (TextView) itemView.findViewById(R.id.price);
        tvMonth = (TextView) itemView.findViewById(R.id.month);
        ll = (LinearLayout) itemView.findViewById(R.id.linearLayout);
    }


}
