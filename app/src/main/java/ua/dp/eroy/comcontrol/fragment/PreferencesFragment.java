package ua.dp.eroy.comcontrol.fragment;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import ua.dp.eroy.comcontrol.R;

public class PreferencesFragment extends PreferenceFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preference);
    }


}
