package ua.dp.eroy.comcontrol.fragment.communal;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Date;

import co.moonmonkeylabs.realmrecyclerview.RealmRecyclerView;
import io.realm.RealmResults;
import ua.dp.eroy.comcontrol.R;
import ua.dp.eroy.comcontrol.adapter.EnergyRVAdapter;
import ua.dp.eroy.comcontrol.model.Energy;
import ua.dp.eroy.comcontrol.utils.RealmController;


public class FragmentEnergy extends CommunalApp {

    private RealmResults<Energy> energyRealmResults;
    private RealmRecyclerView realmRecyclerView;

    private EditText etReadingsThisCold;
    private Button btnPriceAddColdWater;
    private TextView tvPriceEnergy;
    private EnergyRVAdapter energyRVAdapter;

    private TextView rate, readings;
    private ImageView iconCom;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_communal_selected, container, false);

        initView(view);

        btnPriceAddColdWater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etReadingsThisCold.getText().toString().equals("")) {
                    setSnackBar(view, R.string.field_empty);
                } else {
                    calcAndAddToDB();

                }
            }
        });



        return view;
    }


    private void initView(View view) {

        realmRecyclerView = (RealmRecyclerView) view.findViewById(R.id.realm_recycler_view);
        etReadingsThisCold = (EditText) view.findViewById(R.id.etReadingsThisHold);
        btnPriceAddColdWater = (Button) view.findViewById(R.id.btnPriceAddHoldWater);
        tvPriceEnergy = (TextView) view.findViewById(R.id.tvPriceHoldWater);
        rate = (TextView) view.findViewById(R.id.rate);
        readings = (TextView) view.findViewById(R.id.readings);
        iconCom = (ImageView) view.findViewById(R.id.tvIconCom);

//        icon energy
        iconCom.setImageResource(R.drawable.energy);

        energyRealmResults = realm.where(Energy.class).findAllSorted("id", false);
        energyRVAdapter = new EnergyRVAdapter(getActivity(), energyRealmResults, true, true);

        realmRecyclerView.setAdapter(energyRVAdapter);

//        set text title
        readings.setText(getResources().getText(R.string.readings_energy));
    }


    //    calculate energy
    private float getCalcEnergy(int previousValue, int thisValue) {
        String text, textSnack;
        float sum;
        float value100 = 100;
        float value;
        float tmp;
        float difference = thisValue - previousValue;
        if (difference > value100) {
            value = (difference - value100) * price.getEnergyMore100();
            tmp = value100 * price.getEnergyLess100();
            sum = value + tmp;

            text = value100 + " * " + price.getEnergyLess100() + "\n + " + (difference - value100)
                    + " * " + price.getEnergyMore100() + "\n = " + sum;
            textSnack = value100 + " * " + price.getEnergyLess100() + " + " + (difference - value100)
                    + " * " + price.getEnergyMore100() + " = " + sum;

        } else {
            sum = difference * price.getEnergyLess100();
            text = difference + " * " + price.getEnergyLess100() + " = " + sum;
            textSnack = "(" + thisValue + " - " + previousValue + ")" + " * " + price.getEnergyLess100() + " = " + sum;

        }

        setSnackBar(view, textSnack);
        tvPriceEnergy.setText(text);


        return sum;
    }

    private void calcAndAddToDB() {
        int thisValue = Integer.parseInt(etReadingsThisCold.getText().toString());
        // начальное значение
        int previousValue = 0;
// find previous value in BD
        RealmResults<Energy> rr = realm.where(Energy.class).findAll();
        for (Energy en : rr) {
            previousValue = en.getValue();
        }
        float price = getCalcEnergy(previousValue, thisValue);

//                add to db
        insertToDBEnergy(thisValue, price);
//                reset editText
        etReadingsThisCold.setText("");
    }

    //    insert to db energy
    private void insertToDBEnergy(int value, float price) {
        Energy energy = new Energy();
        energy.setValue(value);
        energy.setPrice(price);
        energy.setMonth(String.valueOf(formatDate(new Date())));
        energy.setId(RealmController.getInstance().getColdWater().size() + System.currentTimeMillis());

        realm.beginTransaction();
        realm.copyToRealm(energy);
        realm.commitTransaction();
        energyRVAdapter.notifyDataSetChanged();
    }


    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());


        String prices = prefs.getString("energy", "0.0");

        if (isNumeric(prices)) {
            float pr = Float.parseFloat(prices);

            price.setEnergyLess100(pr);

        } else {
            setSnackBar(R.string.error_energy_less100);
        }

        String prices1 = prefs.getString("energy1", "0.0");
        if (isNumeric(prices1)) {
            float pr1 = Float.parseFloat(prices1);
            price.setEnergyMore100(pr1);
        } else {
            setSnackBar(R.string.error_energy_more100);
        }

        String text = "Цена до 100 кВт: \n" + price.getEnergyLess100() + " грн." +
                "\nЦена от 100 кВт до 600 кВт: \n" + price.getEnergyMore100() + " грн.";

        rate.setText(text);


    }
}
