package ua.dp.eroy.comcontrol.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import ua.dp.eroy.comcontrol.R;
import ua.dp.eroy.comcontrol.fragment.StartSelection;

public class FirstStartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_start);

        //        init toolbar
        Toolbar mActionBarToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mActionBarToolbar);
        mActionBarToolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        mActionBarToolbar.setBackgroundColor(getResources().getColor(R.color.colorDark));

//        Start screen
        FirstStartActivity.this.getSupportFragmentManager().beginTransaction().replace(R.id.content_first_frame, new StartSelection()).commit();


    }
}
