package ua.dp.eroy.comcontrol.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ua.dp.eroy.comcontrol.R;


public class FragmentAbout extends Fragment {

    private TextView version;
    private RelativeLayout rlFb, rlVersion;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragmnet_about, container, false);

        version = (TextView) view.findViewById(R.id.versionApp);
        rlFb = (RelativeLayout) view.findViewById(R.id.rlFB);
        rlVersion = (RelativeLayout) view.findViewById(R.id.rlVersion);

        version.setText(R.string.versionApp);


        rlFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri address = Uri.parse("https://www.facebook.com/groups/326571891027075/");
                Intent openLinkIntent = new Intent(Intent.ACTION_VIEW, address);
                startActivity(openLinkIntent);
            }
        });

        rlVersion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, R.string.versionApp, Snackbar.LENGTH_SHORT).show();
            }
        });

        return view;

    }
}
