package ua.dp.eroy.comcontrol.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import ua.dp.eroy.comcontrol.fragment.PreferencesFragment;

public class SetPreferenceActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction().replace(android.R.id.content,
                new PreferencesFragment()).commit();
    }


}
