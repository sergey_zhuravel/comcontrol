package ua.dp.eroy.comcontrol.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import ua.dp.eroy.comcontrol.R;
import ua.dp.eroy.comcontrol.fragment.SplashFragment;


public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);

        runSplash();

    }

    private void runSplash() {
        SplashFragment splashFragment = new SplashFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.content, splashFragment).addToBackStack(null).commit();
    }
}
