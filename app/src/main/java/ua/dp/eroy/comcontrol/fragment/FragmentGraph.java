package ua.dp.eroy.comcontrol.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import im.dacer.androidcharts.LineView;
import io.realm.RealmResults;
import ua.dp.eroy.comcontrol.R;
import ua.dp.eroy.comcontrol.fragment.communal.CommunalApp;
import ua.dp.eroy.comcontrol.model.ColdWater;
import ua.dp.eroy.comcontrol.model.Energy;
import ua.dp.eroy.comcontrol.model.Gas;
import ua.dp.eroy.comcontrol.model.HotWater;

public class FragmentGraph extends CommunalApp {

    private LineView lvEnergy;
    private LineView lvColdWater;
    private LineView lvHotWater;
    private LineView lvGas;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_graph, container, false);


        initView(view);


        setGraphEnergy(lvEnergy);
        setGraphColdWater(lvColdWater);
        setGraphHotWater(lvHotWater);
        setGraphGas(lvGas);


        return view;
    }

    private void initView(View view) {
        lvEnergy = (LineView) view.findViewById(R.id.graph_energy);
        lvColdWater = (LineView) view.findViewById(R.id.graph_coldWater);
        lvHotWater = (LineView) view.findViewById(R.id.graph_hotWater);
        lvGas = (LineView) view.findViewById(R.id.graph_gas);
    }

    private void setGraphEnergy(LineView lineView) {
        ArrayList<String> valueList = new ArrayList<>();
        ArrayList<ArrayList<Integer>> priceListGraph = new ArrayList<>();
        ArrayList<Integer> priceList = new ArrayList<>();

        RealmResults<Energy> realmResults = realm.where(Energy.class).findAllSorted("id", false);

        for (int i = 0; i < realmResults.size(); i++) {
            valueList.add(String.valueOf(realmResults.get(i).getMonth()));
            priceList.add((int) realmResults.get(i).getPrice());

        }

        priceListGraph.add(priceList);
        if (!priceListGraph.isEmpty() && !valueList.isEmpty()) {
            lineView.setBottomTextList(valueList);
            lineView.setDataList(priceListGraph);
            lineView.setBackgroundColor(getResources().getColor(R.color.colorGrey));
            lineView.setShowPopup(LineView.SHOW_POPUPS_All);
        }
    }

    private void setGraphColdWater(LineView lineView) {
        ArrayList<String> valueList = new ArrayList<>();
        ArrayList<ArrayList<Integer>> priceListGraph = new ArrayList<>();
        ArrayList<Integer> priceList = new ArrayList<>();

        RealmResults<ColdWater> realmResults = realm.where(ColdWater.class).findAllSorted("id", false);

        for (int i = 0; i < realmResults.size(); i++) {
            valueList.add(String.valueOf(realmResults.get(i).getMonth()));
            priceList.add((int) realmResults.get(i).getPrice());

        }

        priceListGraph.add(priceList);

        if (!priceListGraph.isEmpty() && !valueList.isEmpty()) {
            lineView.setBottomTextList(valueList);
            lineView.setDataList(priceListGraph);
            lineView.setBackgroundColor(getResources().getColor(R.color.colorGrey));
            lineView.setShowPopup(LineView.SHOW_POPUPS_All);
        }
    }

    private void setGraphHotWater(LineView lineView) {
        ArrayList<String> valueList = new ArrayList<>();
        ArrayList<ArrayList<Integer>> priceListGraph = new ArrayList<>();
        ArrayList<Integer> priceList = new ArrayList<>();

        RealmResults<HotWater> realmResults = realm.where(HotWater.class).findAllSorted("id", false);

        for (int i = 0; i < realmResults.size(); i++) {
            valueList.add(String.valueOf(realmResults.get(i).getMonth()));
            priceList.add((int) realmResults.get(i).getPrice());

        }

        priceListGraph.add(priceList);

        if (!priceListGraph.isEmpty() && !valueList.isEmpty()) {
            lineView.setBottomTextList(valueList);
            lineView.setDataList(priceListGraph);
            lineView.setBackgroundColor(getResources().getColor(R.color.colorGrey));
            lineView.setShowPopup(LineView.SHOW_POPUPS_All);
        }
    }

    private void setGraphGas(LineView lineView) {
        ArrayList<String> valueList = new ArrayList<>();
        ArrayList<ArrayList<Integer>> priceListGraph = new ArrayList<>();
        ArrayList<Integer> priceList = new ArrayList<>();

        RealmResults<Gas> realmResults = realm.where(Gas.class).findAllSorted("id", false);

        for (int i = 0; i < realmResults.size(); i++) {
            valueList.add(String.valueOf(realmResults.get(i).getMonth()));
            priceList.add((int) realmResults.get(i).getPrice());

        }

        priceListGraph.add(priceList);

        if (!priceListGraph.isEmpty() && !valueList.isEmpty()) {
            lineView.setBottomTextList(valueList);
            lineView.setDataList(priceListGraph);
            lineView.setBackgroundColor(getResources().getColor(R.color.colorGrey));
            lineView.setShowPopup(LineView.SHOW_POPUPS_All);
        }
    }


}
