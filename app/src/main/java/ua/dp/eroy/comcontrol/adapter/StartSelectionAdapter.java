package ua.dp.eroy.comcontrol.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ua.dp.eroy.comcontrol.R;
import ua.dp.eroy.comcontrol.model.Communal;


public class StartSelectionAdapter extends RecyclerView.Adapter<StartSelectionAdapter.ViewHolder> {

    private List<Communal> listCommunal;

    public StartSelectionAdapter(List<Communal> listCommunal) {
        this.listCommunal = listCommunal;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_selection_communal, null);

        ViewHolder viewHolder = new ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final int pos = position;

        holder.textCommunal.setText(listCommunal.get(position).getText());
        holder.imageCommunal.setImageResource(listCommunal.get(position).getImage());
        holder.checkSelected.setChecked(listCommunal.get(position).isSelected());

        holder.checkSelected.setTag(listCommunal.get(position));

        holder.checkSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckBox checkBox = (CheckBox) view;
                Communal communal =(Communal) checkBox.getTag();

                communal.setSelected(checkBox.isChecked());
                listCommunal.get(pos).setSelected(checkBox.isChecked());




            }
        });

    }

    @Override
    public int getItemCount() {
        return listCommunal.size();
    }

    public List<Communal> getCommunalList(){
        return listCommunal;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textCommunal;
        public ImageView imageCommunal;
        public CheckBox checkSelected;

        public Communal communalList;


        public ViewHolder(View itemView) {
            super(itemView);

            textCommunal = (TextView) itemView.findViewById(R.id.textCommunal);
            imageCommunal = (ImageView) itemView.findViewById(R.id.imageCommunal);
            checkSelected = (CheckBox) itemView.findViewById(R.id.chkSelected);

        }
    }

}
