package ua.dp.eroy.comcontrol.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import ua.dp.eroy.comcontrol.R;
import ua.dp.eroy.comcontrol.fragment.communal.FragmentColdWater;
import ua.dp.eroy.comcontrol.fragment.communal.FragmentEnergy;
import ua.dp.eroy.comcontrol.fragment.communal.FragmentGas;
import ua.dp.eroy.comcontrol.fragment.communal.FragmentHotWater;


public class CommunalList extends Fragment implements View.OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setRetainInstance(true);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_communal_list, container, false);

        initView(view);


        return view;
    }

    private void initView(View view) {
        RelativeLayout rlColdWater = (RelativeLayout) view.findViewById(R.id.rlColdWater);
        RelativeLayout rlHotWater = (RelativeLayout) view.findViewById(R.id.rlHotWater);
        RelativeLayout rlEnergy = (RelativeLayout) view.findViewById(R.id.rlEnergy);
        RelativeLayout rlGas = (RelativeLayout) view.findViewById(R.id.rlGas);

        rlHotWater.setOnClickListener(this);
        rlColdWater.setOnClickListener(this);
        rlEnergy.setOnClickListener(this);
        rlGas.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rlColdWater:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new FragmentColdWater()).addToBackStack(null).commit();
                break;
            case R.id.rlHotWater:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new FragmentHotWater()).addToBackStack(null).commit();

                break;

            case R.id.rlEnergy:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new FragmentEnergy()).addToBackStack(null).commit();

                break;
            case R.id.rlGas:
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new FragmentGas()).addToBackStack(null).commit();

                break;

        }


    }


}
