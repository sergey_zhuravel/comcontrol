package ua.dp.eroy.comcontrol.fragment.communal;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Date;

import co.moonmonkeylabs.realmrecyclerview.RealmRecyclerView;
import io.realm.RealmResults;
import ua.dp.eroy.comcontrol.R;
import ua.dp.eroy.comcontrol.adapter.ColdWaterRVAdapter;
import ua.dp.eroy.comcontrol.model.ColdWater;
import ua.dp.eroy.comcontrol.utils.RealmController;


public class FragmentColdWater extends CommunalApp {

    private RealmResults<ColdWater> coldWaterRealmResults;
    private RealmRecyclerView realmRecyclerView;
    private EditText etReadingsThisCold;
    private Button btnPriceAddColdWater;
    private TextView tvPriceColdWater;
    private ColdWaterRVAdapter coldWaterRVAdapter;

    private TextView rate, readings;
    private ImageView iconCom;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_communal_selected, container, false);
        initView(view);
        setHasOptionsMenu(true);


        btnPriceAddColdWater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etReadingsThisCold.getText().toString().equals("")) {
                    setSnackBar(view, R.string.field_empty);
                } else {
                    calcAndAddToDB();

                }
            }
        });



        return view;
    }

    private void calcAndAddToDB() {
        int thisValue = Integer.parseInt(etReadingsThisCold.getText().toString());
//        начальное значение
        int previousValue = 0;
//        find previous value in BD
        RealmResults<ColdWater> rr = realm.where(ColdWater.class).findAll();
        for (ColdWater cw : rr) {
            previousValue = cw.getValue();
        }
        float price = getCalcColdWater(previousValue, thisValue);
//        add to db
        insertToDBColdWater(thisValue, price);
//        reset editText
        etReadingsThisCold.setText("");
    }



    private void initView(View view) {
        realmRecyclerView = (RealmRecyclerView) view.findViewById(R.id.realm_recycler_view);
        etReadingsThisCold = (EditText) view.findViewById(R.id.etReadingsThisHold);
        btnPriceAddColdWater = (Button) view.findViewById(R.id.btnPriceAddHoldWater);

        tvPriceColdWater = (TextView) view.findViewById(R.id.tvPriceHoldWater);
        rate = (TextView) view.findViewById(R.id.rate);
        readings = (TextView) view.findViewById(R.id.readings);
        iconCom = (ImageView) view.findViewById(R.id.tvIconCom);

        iconCom.setImageResource(R.drawable.coldwater);

        coldWaterRealmResults = realm.where(ColdWater.class).findAllSorted("id", false);
        coldWaterRVAdapter = new ColdWaterRVAdapter(getActivity(), coldWaterRealmResults, true, true);

        realmRecyclerView.setAdapter(coldWaterRVAdapter);

//        set text title
        readings.setText(getResources().getText(R.string.readings_cold_water));
    }


    //    calculate cold water
    public float getCalcColdWater(int previousValue, int thisValue) {
        float value = (thisValue - previousValue) * price.getColdWater();
        String text = "(" + thisValue + " - " + previousValue + ") " + " * " + price.getColdWater() + " = " + value;
        setSnackBar(view, text);
        tvPriceColdWater.setText(text);
        return value;
    }


    private void insertToDBColdWater(int value, float price) {
        ColdWater coldWater = new ColdWater();
        coldWater.setValue(value);
        coldWater.setPrice(price);
        coldWater.setMonth(String.valueOf(formatDate(new Date())));
        coldWater.setId(RealmController.getInstance().getColdWater().size() + System.currentTimeMillis());

        realm.beginTransaction();
        realm.copyToRealm(coldWater);
        realm.commitTransaction();
        coldWaterRVAdapter.notifyDataSetChanged();

    }


    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());

        String prices = prefs.getString("coldwater", "0.0");
        if (isNumeric(prices)) {
            float pr = Float.parseFloat(prices);
            price.setColdWater(pr);
        } else {
            setSnackBar(R.string.error_cold_water);
        }

        //    set text rate cold water
        rate.setText(String.valueOf(price.getColdWater()));

    }
}
